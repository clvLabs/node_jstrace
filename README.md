# jstrace #

## Node.js debug trace utils ##

Add a little color to console.log() by using chalk:

* showData / data / d
* showHighlight / highlight / high / hi / h
* showInfo / info / i
* showWarning / warning / warn / w
* showError / error / err / e

Wait for a keypress

* waitForKeyPress / wait

### How to install it ###

```
npm install --save git+http://bitbucket.org/clvLabs/node_jstrace.git
```

### How to use it ###

```
var dbg = require('jstrace');

dbg.d('Data color');
dbg.h('Highlight color');
dbg.i('Info color');
dbg.w('Warning color');
dbg.e('Error color');

dbg.wait();
```

### Creating docs with JSDoc ###

Go to <your_project_folder>/node_modules/jstrace/ and call
```
npm install jsdoc
./node_modules/bin/jsdoc main.js readme.md -d docs --verbose
```
This will create a **docs** folder containing the module documentation.