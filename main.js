/** @file Node.js debug trace utility
 * 
 * @version 0.1.0
 * @author Tony Aguilar
 * @copyright 2015
 */

/** Debug trace utils<br>
 * Exported methods:<br>
 * <ul>	
 * <li>showData / data / d</li>
 * <li>showHighlight / highlight / high / hi / h</li>
 * <li>showInfo / info / i</li>
 * <li>showWarning / warning / warn / w</li>
 * <li>showError / error / err / e</li>
 * <li>waitForKeyPress / wait</li>
 * </ul>
 * @module main
 */
const util = require('util');
const chalk = require('chalk');

const dataColor = chalk.bold.gray;
const highlightColor = chalk.bold.white;
const infoColor = chalk.bold.green;
const warningColor = chalk.bold.yellow.bgBlue;
const errorColor = chalk.bold.yellow.bgRed;

var arrSlice = Array.prototype.slice;

/** Sends a request to the service, parses the result
 * and saves the "cache" file.
 *
 * @param {chalkColor} color Color to use
 * @param {arguments} args Parameters for console.log()
 */
showLog = function(color, args) {
	console.log(color(util.format.apply(null, arrSlice.call(args))));
};

/** Shows a message using "Data" color
 *
 * @param ... Same parameters as for console.log()
 */
showData = function()		{	showLog(dataColor, arguments);		};

/** Shows a message using "Highlight" color
 *
 * @param ... Same parameters as for console.log()
 */
showHighlight = function()	{	showLog(highlightColor, arguments);	};

/** Shows a message using "Info" color
 *
 * @param ... Same parameters as for console.log()
 */
showInfo = function()		{	showLog(infoColor, arguments);		};

/** Shows a message using "Warning" color
 *
 * @param ... Same parameters as for console.log()
 */
showWarning = function()	{	showLog(warningColor, arguments);	};

/** Shows a message using "Error" color
 *
 * @param ... Same parameters as for console.log()
 */
showError = function()		{	showLog(errorColor, arguments);		};


/** Waits for a keypress
 */
waitForKeyPress = function(){
	showInfo('Press any key to exit');

	process.stdin.setRawMode(true);
	process.stdin.resume();
	process.stdin.on('data', process.exit.bind(process, 0));
};

module.exports = {
	showData: showData,
	data: showData,
	d: showData,

	showHighlight: showHighlight,
	highlight: showHighlight,
	high: showHighlight,
	hi: showHighlight,
	h: showHighlight,

	showInfo: showInfo,
	info: showInfo,
	i: showInfo,

	showWarning: showWarning,
	warning: showWarning,
	warn: showWarning,
	w: showWarning,

	showError: showError,
	error: showError,
	err: showError,
	e: showError,

	waitForKeyPress:  waitForKeyPress,
	wait:  waitForKeyPress,
};